import { Message } from 'element-ui';
//绘制标记
export class sign {
  constructor() {
    this.handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
  }
  startDraw(fn) {
    this.handler.setInputAction((evt) => { //单机开始绘制
      var cartesian;
      //世界坐标
      cartesian = getCatesian3FromPX(evt.position, viewer);
      if (!cartesian) return;
      //转地理坐标
      var ellipsoid = viewer.scene.globe.ellipsoid;
      var cartographic = ellipsoid.cartesianToCartographic(cartesian);
      var lat = Cesium.Math.toDegrees(cartographic.latitude);
      var lon = Cesium.Math.toDegrees(cartographic.longitude);
      // var alt=cartographic.height;
      // let po = Cesium.Cartesian3.fromDegrees(lon, lat,alt);
      fn({ lon, lat })
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
  }
  destroy() {
    this.handler.destroy();
  }
}


//测距
export class ranging {
  constructor() {
    this.positions = [];
    this.dianArr = [];
    this.handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
  }
  startDraw(fn) {
    this.handler.setInputAction((evt) => { //单机开始绘制
      var cartesian;
      //世界坐标
      cartesian = getCatesian3FromPX(evt.position, viewer);
      if (!cartesian) return;
      // 画点
      let dian = creatPoint(cartesian);
      this.positions.push(cartesian);
      this.dianArr.push(dian)
        //创建线
        // if (this.positions.length == 2) {
        //   creatLine(this.positions);
        //   console.log(coordinate(this.positions[0]), coordinate(this.positions[1]), '经纬度')
        //   fn()
        // }
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
    this.handler.setInputAction((evt) => { //移动时绘制面
      creatLine(this.positions);
      this.positions.forEach(val => {
        console.log(coordinate(val), '经纬度')
      })
      fn()
    }, Cesium.ScreenSpaceEventType.RIGHT_CLICK);
  }
  destroy() {
    this.dianArr.forEach(val => {
      viewer.entities.remove(val);
    })
    this.positions = [];
    this.handler.destroy();
  }
}

//保障
export class guarantee {
  constructor() {
    this.handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
    this.enList = [];
  }
  startDraw(fn) {
    Message.success('请选择保障车');
    this.handler.setInputAction((evt) => { //单机开始绘制
      let pick = viewer.scene.pick(evt.position);
      if (pick && pick.id) {
        let en = pick.id;
        if (en.datas && en.datas.category == 'jf' && en.datas.type == 'qx' && this.enList.length == 0) {
          this.enList.push(en);
          Message.success('请选择被保障TK');
          return
        }
        if (en.datas && en.datas.category == 'jf' && en.datas.type == 'tanke' && this.enList.length == 1) {
          this.enList.push(en);
          fn && fn(this.enList);
          Message.success('保障完毕');
          return
        }
        if (this.enList.length == 0) Message.warning('请选择保障车');
        if (this.enList.length == 1) Message.warning('请选择我方tk');
      } else {
        Message.warning('请选择模型')
      }
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
  }
  destroy() {
    this.enList = [];
    this.handler.destroy();
  }
}

//攻击
export class attack {
  constructor() {
    this.handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
    this.enList = [];
  }
  startDraw(fn) {
    Message.success('请先选择我方TK');
    this.handler.setInputAction((evt) => { //单机开始绘制
      let pick = viewer.scene.pick(evt.position);
      if (pick && pick.id) {
        let en = pick.id;
        //判断点击的坦克
        if (en.datas && en.datas.category == 'jf' && en.datas.type == 'tanke' && this.enList.length == 0) {
          this.enList.push(en);
          Message.success('请选择d方TK');
          return
        }
        if (en.datas && en.datas.category == 'df' && en.datas.type == 'tanke' && this.enList.length == 1) {
          this.enList.push(en);
          fn && fn(this.enList);
          Message.success('选择完毕，开始攻击！');
          return
        }
        if (this.enList.length == 0) Message.warning('请选择我方tk');
        if (this.enList.length == 1) Message.warning('请选择dftk');
      } else {
        Message.warning('请选择TK')
      }
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
  }
  destroy() {
    this.enList = [];
    this.handler.destroy();
  }
}

//取消标注
export class cancel {
  constructor() {
    this.handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
  }
  startDraw(fn) {
    Message.warning('请选择敌方标注');
    this.handler.setInputAction((evt) => { //单机开始绘制
      let pick = viewer.scene.pick(evt.position);
      if (pick && pick.id) {
        let en = pick.id;
        if (en.datas && en.datas.category == 'df') {
          fn(en);
          return
        }
      };
      Message.warning('请选择敌方标注');
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
  }
  destroy() {
    this.handler.destroy();
  }
}

//复活 死亡
export class otherOperations {
  constructor() {
    this.handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
  }
  startDraw(fn) {
    Message.warning('请选择tk进行操作');
    this.handler.setInputAction((evt) => { //单机开始绘制
      let pick = viewer.scene.pick(evt.position);
      if (pick && pick.id) {
        let en = pick.id;
        if (en.datas && en.datas.type == 'tanke') {
          fn(en);
          return
        }
      };
      Message.warning('请选择tk');
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
  }
  destroy() {
    this.handler.destroy();
  }
}
//创建点
function creatPoint(cartesian) {
  var point = viewer.entities.add({
    position: cartesian,
    billboard: {
      image: "./static/point.png",
      verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
      heightReference: Cesium.HeightReference.CLAMP_TO_GROUND,
    }
  });
  return point;
}
//创建线
function creatLine(position) {
  let line = viewer.entities.add({
    position: position[1],
    type: 'ceju',
    label: {

      // This callback updates the length to print each frame.
      text: getSpaceDistance(position) + 'm',
      font: "20px sans-serif"
    },
    polyline: {
      positions: position,
      width: 10.0,
      material: new Cesium.PolylineGlowMaterialProperty({
        color: new Cesium.Color(246 / 255, 7 / 255, 7 / 255, 1),
        glowPower: 0.25,
      }),
      clampToGround: true
    },

  });
  return line;
}
//空间两点距离计算函数
function getSpaceDistance(positions) {
  var distance = 0;
  for (var i = 0; i < positions.length - 1; i++) {

    var point1cartographic = Cesium.Cartographic.fromCartesian(positions[i]);
    var point2cartographic = Cesium.Cartographic.fromCartesian(positions[i + 1]);
    /**根据经纬度计算出距离**/
    var geodesic = new Cesium.EllipsoidGeodesic();
    geodesic.setEndPoints(point1cartographic, point2cartographic);
    var s = geodesic.surfaceDistance;
    //返回两点之间的距离
    s = Math.sqrt(Math.pow(s, 2) + Math.pow(point2cartographic.height - point1cartographic.height, 2));
    distance = distance + s;
  }
  return distance.toFixed(2);
}
//获取位置
function getCatesian3FromPX(px, viewer) {
  // var picks = viewer.scene.drillPick(px);
  // viewer.render();
  var cartesian;
  // var isOn3dtiles = true;
  // for (var i = 0; i < picks.length; i++) {
  //   if ((picks[i] && picks[i].primitive) || picks[i] instanceof Cesium.Cesium3DTileFeature) { //模型上拾取
  //     isOn3dtiles = true;
  //   }
  // }
  // if (isOn3dtiles) {
  //   cartesian = viewer.scene.pickPosition(px);
  // } else {
  var ray = viewer.camera.getPickRay(px);
  if (!ray) return null;
  cartesian = viewer.scene.globe.pick(ray, viewer.scene);
  // }
  return cartesian;
}

function coordinate(item) {
  var cartesian33 = new Cesium.Cartesian3(item.x, item.y, item.z);
  var cartographic = Cesium.Cartographic.fromCartesian(cartesian33);
  var lat = Cesium.Math.toDegrees(cartographic.latitude);
  var lng = Cesium.Math.toDegrees(cartographic.longitude);
  return { lng: lng, lat: lat }
}