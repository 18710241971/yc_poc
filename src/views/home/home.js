import cesiumFL from '@/utils/cesiumFL'
// 用于引入全局变量
import store from "@/store/index";
//弹窗
import Popup from '@/utils/Popup.js'
//确认弹窗
import { MessageBox } from 'element-ui';
export default class cesiumEntity {
  constructor(id) {
    // 全局存储车辆实时位置数据
    this.posMap = new Map();
    //全局的初始化方法
    initViewerIntnet(id, false);
    // 相机飞行
    cesiumFL.cameraFlying(cameraPoint);
    //绘制汽车路径
    // this.createVehicleByPath(carPath);
    //绘制扇形
    //事件
    this.initEvent();
    this.cameraTS();
    //用于点击坦克状态弹框
    this.statusList = null;
    //控制弹窗能否开启 开关
    this.popupSwitch = true;
    //存储需要显示删除的实体
    this.showDeleteEns = [];
    //添加流动材质
    cesiumFL.initPolylineTrailLinkMaterialProperty({ flowImage: `./assets/img/lv.png` });
  }
  //初始化事件
  initEvent() {
    var handler;
    handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
    // 鼠标左击
    // handler.setInputAction((e) => {
    //   let pick = viewer.scene.pick(e.endPosition);
    //   if (this.statusList) {
    //     this.statusList.close();
    //     this.statusList = null;
    //   };
    //   if (pick && pick.id) {
    //     let en = pick.id;
    //     //判断点击的
    //     if (this.popupSwitch && en.datas && en.datas.type == 'jd') {
    //       //弹窗
    //       this.statusList = new Popup({
    //         content: `
    //           <div class="box2">
    //             <div class="ys-block">
    //                 <div style='width:200px' class="ys-con">
    //                    asdasdasd
    //                 </div>
    //             </div>
    //         </div>`,
    //         entity: en,
    //         id: 'jdPopup',
    //         top: 40
    //       })
    //     }
    //   }
    // }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
    handler.setInputAction((e) => {
      let pick = viewer.scene.pick(e.position);
      if (this.statusList) {
        this.statusList.close();
        this.statusList = null;
      };
      if (pick && pick.id) {
        let en = pick.id;
        console.log(en)
        //判断点击的
        if (this.popupSwitch && en.datas && en.datas.type == 'jd') {
          //弹窗
          this.statusList = new Popup({
            content: `
                <div class="box2">
                  <div class="ys-block">
                      <div style='width:220px' class="ys-con">
                         ${en.datas.data.base_name}
                      </div>
                      <div style='width:220px' class="ys-con">
                        国别:${en.datas.data.base_country}
                      </div>
                  </div>
              </div>`,
            entity: en,
            id: 'jdPopup',
            top: 10,
            close: true
          })
        }
      }
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
    //双击事件
    handler.setInputAction((e) => {
      let pick = viewer.scene.pick(e.position);
      if (pick && pick.id) {
        let en = pick.id;
        if (en.datas && (en.datas.type == 'bz' || en.datas.type == 'dj')) {
          console.log(en);
          MessageBox.confirm('是否取消此关系线？', '提示', {
            confirmButtonText: '确定',
            cancelButtonText: '取消',
            type: 'warning'
          }).then(() => {
            viewer.entities.remove(en);
          })
        }
        //测距线ceju
        if (en.type == 'ceju') {
          viewer.entities.remove(en);
        }
      }
    }, Cesium.ScreenSpaceEventType.LEFT_DOUBLE_CLICK);
  }
  //删除敌方实体坦克
  removeDFTK(en) {
    viewer.entities.remove(en);
  }
  //相机调试
  cameraTS() {
    //缩小需要显示的
    // let en = viewer.entities.add({
    //   position: Cesium.Cartesian3.fromDegrees(
    //     centralPoint.lon,
    //     centralPoint.lat
    //   ),
    //   billboard: {
    //     image: `./assets/zhbzjd.png`,
    //     heightReference: Cesium.HeightReference.CLAMP_TO_GROUND,
    //     pixelOffset: new Cesium.Cartesian2(10, 20)
    //   }
    // });
    //视角调试方法
    viewer.scene.camera.moveEnd.addEventListener((e) => {
      var camera = viewer.camera;
      Cesium.Math.toDegrees(viewer.camera.heading)
      Cesium.Math.toDegrees(viewer.camera.pitch) //Cesium.Math.toDegrees作用是把弧度转换成度数
      //将笛卡尔坐标转化为经纬度坐标
      var catographic = Cesium.Cartographic.fromCartesian(camera.position);
      // 此处是经纬度，单位：度。
      var longitude = Number(Cesium.Math.toDegrees(catographic.longitude).toFixed(6));
      var latitude = Number(Cesium.Math.toDegrees(catographic.latitude).toFixed(6));
      var height = Number(catographic.height.toFixed(2));
      console.log("相机视角位置", longitude, latitude, height, camera);
      // this.heightUpdate(en, height)
    })
  }
  //根据高度显示不一样的效果
  heightUpdate(en, height) {
    if (height < 35832.67) {
      this.heighIsShow(en, true)
    } else {
      this.heighIsShow(en, false)
    }
  }
  heighIsShow(en, is) {
    viewer.entities.values.forEach(item => {
      item.show = is
    });
    en.show = !is;
  }
  //创建基地
  createJd(params) {
    let en = viewer.entities.add({
      position: Cesium.Cartesian3.fromDegrees(
        params.base_lon,
        params.base_lat,
        40
      ),
      datas: {
        type: 'jd',
        data: params
      },
      // ellipse: {
      //   semiMinorAxis: 400.0,
      //   semiMajorAxis: 400.0,
      //   material: new Cesium.ImageMaterialProperty({
      //     image: `./assets/${params.type}.png`,
      //     repeat: new Cesium.Cartesian2(1, 1)
      //   })
      // }
      billboard: {
        image: `./assets/${params.type}.png`,
        scale: 0.12,
        // heightReference: Cesium.HeightReference.CLAMP_TO_GROUND,
        // pixelOffset: new Cesium.Cartesian2(10, 20)
      }
    });
    return en
  }
  //报警波
  createBj(params) {
    return cesiumFL.addRipples(params)
  }
  //入侵变红
  rqStat(en, is) {
    if (is) {
      en.model.color = Cesium.Color.fromAlpha(Cesium.Color.RED, 0.9);
    } else {
      en.model.color = null;
    }
  }

  //根据实例画动态线
  enLine(enList, type, id) {

    let en1 = enList[0];
    let en2 = enList[1];

    let enL = viewer.entities.add({
      id: id ? id : en1.id + en2.id,
      datas: {
        type
      },
      polyline: {
        positions: new Cesium.CallbackProperty((time, result) => {
          try {
            let pos = en1._position.getValue(time, null);
            let pos2 = en2._position.getValue(time, null);
            // console.log(time, pos, pos2, 'xy');
            if (pos && pos2) {
              return [pos, pos2];
            } else {
              return null
            }
          } catch (error) {
            return null;
          }
        }, false),
        width: 10.0,
        material: new Cesium.PolylineGlowMaterialProperty({
          color: new Cesium.Color(24 / 255, 148 / 255, 242 / 255, 1),
          glowPower: 0.25,
        }),
        // material: new Cesium.PolylineTrailLinkMaterialProperty(type == 'bz' ? Cesium.Color.GREEN : Cesium.Color.RED, 3000),
      },
    });
    return enL
  }
  //计算heading
  getHeading(pointA, pointB) {
    //建立以点A为原点，X轴为east,Y轴为north,Z轴朝上的坐标系
    const transform = Cesium.Transforms.eastNorthUpToFixedFrame(pointA);
    //向量AB
    const positionvector = Cesium.Cartesian3.subtract(pointB, pointA, new Cesium.Cartesian3());
    //因transform是将A为原点的eastNorthUp坐标系中的点转换到世界坐标系的矩阵
    //AB为世界坐标中的向量
    //因此将AB向量转换为A原点坐标系中的向量，需乘以transform的逆矩阵。
    const vector = Cesium.Matrix4.multiplyByPointAsVector(Cesium.Matrix4.inverse(transform, new Cesium.Matrix4()), positionvector, new Cesium.Cartesian3());
    //归一化
    const direction = Cesium.Cartesian3.normalize(vector, new Cesium.Cartesian3());
    //heading
    const heading = Math.atan2(direction.y, direction.x) - Cesium.Math.PI_OVER_TWO;
    return Cesium.Math.TWO_PI - Cesium.Math.zeroToTwoPi(heading);
  }
  //计算Pitch
  getPitch(pointA, pointB) {
    let transfrom = Cesium.Transforms.eastNorthUpToFixedFrame(pointA);
    const vector = Cesium.Cartesian3.subtract(pointB, pointA, new Cesium.Cartesian3());
    let direction = Cesium.Matrix4.multiplyByPointAsVector(Cesium.Matrix4.inverse(transfrom, transfrom), vector, vector);
    Cesium.Cartesian3.normalize(direction, direction);
    //因为direction已归一化，斜边长度等于1，所以余弦函数等于direction.z
    return Cesium.Math.PI_OVER_TWO - Cesium.Math.acosClamped(direction.z);
  }
  //两点之间距离
  getSpaceDistance(positions) {
    var distance = 0;
    for (var i = 0; i < positions.length - 1; i++) {

      var point1cartographic = Cesium.Cartographic.fromCartesian(positions[i]);
      var point2cartographic = Cesium.Cartographic.fromCartesian(positions[i + 1]);
      /**根据经纬度计算出距离**/
      var geodesic = new Cesium.EllipsoidGeodesic();
      geodesic.setEndPoints(point1cartographic, point2cartographic);
      var s = geodesic.surfaceDistance;
      //console.log(Math.sqrt(Math.pow(distance, 2) + Math.pow(endheight, 2)));
      //返回两点之间的距离
      s = Math.sqrt(Math.pow(s, 2) + Math.pow(point2cartographic.height - point1cartographic.height, 2));
      distance = distance + s;
    }
    return distance.toFixed(2);
  }
  //计算角度
  coursePitchAngle(pointA, pointB) {
    //以a点为原点建立局部坐标系（东方向为x轴,北方向为y轴,垂直于地面为z轴），得到一个局部坐标到世界坐标转换的变换矩阵
    var localToWorld_Matrix = Cesium.Transforms.eastNorthUpToFixedFrame(pointA);
    //求世界坐标到局部坐标的变换矩阵
    var worldToLocal_Matrix = Cesium.Matrix4.inverse(localToWorld_Matrix, new Cesium.Matrix4());
    //a点在局部坐标的位置，其实就是局部坐标原点
    var localPosition_A = Cesium.Matrix4.multiplyByPoint(worldToLocal_Matrix, pointA, new Cesium.Cartesian3());
    //B点在以A点为原点的局部的坐标位置
    var localPosition_B = Cesium.Matrix4.multiplyByPoint(worldToLocal_Matrix, pointB, new Cesium.Cartesian3());
    //弧度
    var angle = Math.atan2((localPosition_B.z - localPosition_A.z), (localPosition_B.x - localPosition_A.x))
    //角度
    var theta = angle * (180 / Math.PI);
    // console.log(theta, 'ss')
    // if (theta < 0) {
    //   theta = theta + 360;
    // }
    return theta;
  }
  /**
   * 根据传进来的数据绘制小车路径
   * @param {Object} pos 新来的位置
   */
  moveVehicleByMqtt(msg) {
    let { id, lat, lon } = msg;
    if (!this.posMap.get(id) && lat && lon) {
      // 如果这是一辆新车
      // 初始化SampledPositionProperty
      let property = new Cesium.SampledPositionProperty();
      let timeStart = new Cesium.JulianDate.now();
      let posStart = Cesium.Cartesian3.fromDegrees(lon, lat);
      property.addSample(timeStart, posStart);

      // 构造虚拟的两个额外的点
      let timeStart1 = Cesium.JulianDate.addSeconds(
        new Cesium.JulianDate.now(),
        0.5,
        new Cesium.JulianDate()
      );

      let posStart1 = new Cesium.Cartesian3(
        posStart.x + 0.001,
        posStart.y + 0.001,
        posStart.z + 0.001
      );
      property.addSample(timeStart1, posStart1);

      let timeStart2 = Cesium.JulianDate.addSeconds(
        new Cesium.JulianDate.now(),
        0.9,
        new Cesium.JulianDate()
      );

      let posStart2 = new Cesium.Cartesian3(
        posStart.x + 0.002,
        posStart.y + 0.002,
        posStart.z + 0.002
      );
      property.addSample(timeStart2, posStart2);

      this.posMap.set(id, property);

      let currentEntity = viewer.entities.add({
        id: id,
        datas: {},
        // 当前时间和结果值
        position: property,
        // orientation: new Cesium.VelocityOrientationProperty(property),
        // 用模型
        billboard: {
          image: `./assets/fly.png`,
          scale: 0.6,
        }
      });
    } else {
      // 如果这是一辆现有的车
      let currenttime = Cesium.JulianDate.addSeconds(
        new Cesium.JulianDate.now(),
        1.0,
        new Cesium.JulianDate()
      );
      let currentPosition = Cesium.Cartesian3.fromDegrees(lon, lat);
      this.posMap.get(id).addSample(currenttime, currentPosition);
    }
  }
  //计算方向
  updateOD(en, lon, lat) {
    let oldLon = en.datas.oldLon;
    let oldLat = en.datas.oldLat;
    if (oldLon == lon && oldLat == lat) {
      if (!isNaN(en.datas.oldJ)) {
        var viewModel = {
          heading: -(en.datas.oldJ / 360) * 6.28,
          // heading: 3.04,
          pitch: 0.0,
          roll: 0.0,
        };
        en.orientation = Cesium.Transforms.headingPitchRollQuaternion(
          Cesium.Cartesian3.fromDegrees(lon, lat, 170),
          new Cesium.HeadingPitchRoll(viewModel.heading, viewModel.pitch, viewModel.roll) // heading,pitch, roll
        )
      } else {
        var viewModel = {
          heading: -1.57,
          // heading: 3.04,
          pitch: 0.0,
          roll: 0.0,
        };
        en.orientation = Cesium.Transforms.headingPitchRollQuaternion(
          Cesium.Cartesian3.fromDegrees(lon, lat, 170),
          new Cesium.HeadingPitchRoll(viewModel.heading, viewModel.pitch, viewModel.roll) // heading,pitch, roll
        )
      }
    } else {
      setTimeout(() => { en.orientation = new Cesium.VelocityOrientationProperty(en.position); }, 400)
      en.datas.oldJ = this.courseAngle(oldLon, oldLat, lon, lat);
      en.datas.oldLon = lon;
      en.datas.oldLat = lat;
    }
  }
  courseAngle(lng_a, lat_a, lng_b, lat_b) {
    //以a点为原点建立局部坐标系（东方向为x轴,北方向为y轴,垂直于地面为z轴），得到一个局部坐标到世界坐标转换的变换矩阵
    var localToWorld_Matrix = Cesium.Transforms.eastNorthUpToFixedFrame(new Cesium.Cartesian3.fromDegrees(lng_a, lat_a));
    //求世界坐标到局部坐标的变换矩阵
    var worldToLocal_Matrix = Cesium.Matrix4.inverse(localToWorld_Matrix, new Cesium.Matrix4());
    //a点在局部坐标的位置，其实就是局部坐标原点
    var localPosition_A = Cesium.Matrix4.multiplyByPoint(worldToLocal_Matrix, new Cesium.Cartesian3.fromDegrees(lng_a, lat_a), new Cesium.Cartesian3());
    //B点在以A点为原点的局部的坐标位置
    var localPosition_B = Cesium.Matrix4.multiplyByPoint(worldToLocal_Matrix, new Cesium.Cartesian3.fromDegrees(lng_b, lat_b), new Cesium.Cartesian3());
    //弧度
    var angle = Math.atan2((localPosition_B.y - localPosition_A.y), (localPosition_B.x - localPosition_A.x))
    //角度
    var theta = angle * (180 / Math.PI);
    if (theta < 0) {
      theta = theta + 360;
    }
    return theta;
  }
  //获取实体
  getEntitieId(id) {
    return viewer.entities.getById(id)
  }
  creatJb(params) {
    var point = viewer.entities.add({
      // id: params.id,
      datas: {
        type: params.type,
        category: 'df'
      },
      position: Cesium.Cartesian3.fromDegrees(
        params.lon,
        params.lat
      ),
      billboard: {
        scale: 0.5,
        image: `./assets/${params.type}.png`,
        heightReference: Cesium.HeightReference.CLAMP_TO_GROUND
      }
    });
    return point;
  }
  //创建 点
  creatBj(params) {
    var point = viewer.entities.add({
      id: params.id,
      position: Cesium.Cartesian3.fromDegrees(
        params.lon,
        params.lat
      ),
      billboard: {
        image: `/static/point.png`,
        heightReference: Cesium.HeightReference.CLAMP_TO_GROUND,
        pixelOffset: new Cesium.Cartesian2(10, 20)
      }
    });
    return point;
  }
  cameraFlying(params) {
    if (!params.duration) {
      // 普通相机视角
      viewer.camera.setView({
        destination: Cesium.Cartesian3.fromDegrees(params.lon, params.lat, params.h),
        orientation: {
          heading: params.heading || 0.0,
          pitch: params.pitch || 0.0,
          roll: params.roll || 0.0,
        }
      });
    } else {
      // 相机飞行动画
      viewer.camera.flyTo({
        destination: Cesium.Cartesian3.fromDegrees(
          params.lon, params.lat, params.h
        ),
        duration: params.duration || 0.0
      });
    }
  }
  //创建图标
  createTB(params) {
    let en = viewer.entities.add({
      position: Cesium.Cartesian3.fromDegrees(
        params.lo[0],
        params.lo[1]
      ),
      datas: {
        type: 'jd',
        data: params
      },
      // ellipse: {
      //   semiMinorAxis: 400.0,
      //   semiMajorAxis: 400.0,
      //   material: new Cesium.ImageMaterialProperty({
      //     image: `./assets/${params.type}.png`,
      //     repeat: new Cesium.Cartesian2(1, 1)
      //   })
      // }
      billboard: {
        image: params.img,
        scale: params.scale || 1,
        // heightReference: Cesium.HeightReference.CLAMP_TO_GROUND,
        // pixelOffset: new Cesium.Cartesian2(10, 20)
      }
    });
    this.showDeleteEns.push(en)
    return en
  }
  //渔船
  createYC(params) {
    let x = 0.05 * parseInt(Math.random()*20);
    // let x = parseInt(Math.random()*10)/10;
    let flog = true;
    // let en = viewer.entities.add({
    //   position: Cesium.Cartesian3.fromDegrees(params.lo[0], params.lo[1]),
    //   point: {
    //     show: true, // default
    //     color: new Cesium.CallbackProperty(function () {
    //       // x = Math.floor(x * 100) / 100;
    //       console.log(x)
    //       if (flog) {
    //         x = x - 0.05;
    //         if (x <= 0) {
    //           flog = false;
    //         }
    //       } else {
    //         x = x + 0.05;
            
    //         if (x >= 1) {
    //           flog = true;
    //         }
    //       }
    //       return Cesium.Color[params.color].withAlpha(x);
    //     }, false),
    //     // scale:50000,
    //     pixelSize: 20, // default: 1
    //     outlineWidth: 1
    //   }
    // });
    let en = cesiumFL.drawRippleEnt(params.lo[0], params.lo[1],100,10000,Cesium.Color[params.color])
    this.showDeleteEns.push(en)
    return en
  }
  //清除需要删除的实体
  DeleteEns() {
    this.showDeleteEns.forEach(val => {
      this.removeDFTK(val)
    })
    this.showDeleteEns = [];
  }
  destroy() {
    viewer.destroy();
    // viewer1.destroy();
    // clearInterval(this.sceneTwo.timeOut)
  }
}