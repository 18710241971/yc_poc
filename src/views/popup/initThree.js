import RThree from './RThree'
//引入three。js
import * as THREE from 'three'
//
import axios from 'axios'
export default class initApp extends RThree {
	constructor(container, options) {
		super(container, options);
		this.created(container, options);
	}
	created(container, options) {
		//动画数组
		this.meshArr = [];
		//暂存实体数组
		this.zcMeshArr = [];
		//初始化
		this.init(options);
		this.addTexture()
		//加载控制器
		this.initOrbitControls();

		//渲染
		this.render(() => {
			if (this.meshArr.length > 0) {

				this.meshArr.forEach(mesh => {
					mesh._s += 0.007;
					mesh.scale.set(
						mesh.size * mesh._s,
						mesh.size * mesh._s,
						mesh.size * mesh._s
					);
					if (mesh._s <= 1.5) {
						mesh.material.opacity = (mesh._s - 1) * 2; //2等于1/(1.5-1.0)，保证透明度在0~1之间变化
					} else if (mesh._s > 1.5 && mesh._s <= 2) {
						mesh.material.opacity = 1 - (mesh._s - 1.5) * 2; //2等于1/(2.0-1.5) mesh缩放2倍对应0 缩放1.5被对应1
					} else {
						mesh._s = 1.0;
					}
				})
			}
		});
		this.loadDq();
		this.loadLL()
		//相机位置
		this.camera.position.x = -23.73775736529673;
		this.camera.position.y = 41.58299392485902;
		this.camera.position.z = -71.98814148583051;
		//加载中国板块
		this.axiosChinaJson();

	}
	loadDq() {
		var earth, cloud;
		// stats = new Stats();
		// stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
		// document.body.appendChild( stats.dom );

		// Earth terrain
		var earth_texture = new THREE.TextureLoader().load("/tx/earth.jpeg");
		var earth_bump = new THREE.TextureLoader().load("/tx/bump.jpeg");
		var earth_specular = new THREE.TextureLoader().load("/tx/spec.jpeg");
		var earth_geometry = new THREE.SphereGeometry(30, 32, 32);
		var earth_material = new THREE.MeshPhongMaterial({
			shininess: 40,
			bumpScale: 1,
			map: earth_texture,
			bumpMap: earth_bump,
			specularMap: earth_specular
		});
		earth = new THREE.Mesh(earth_geometry, earth_material);
		this.scene.add(earth);

		// Earth cloud
		// var cloud_texture  = new THREE.TextureLoader().load('/tx/cloud.png');
		// var cloud_geometry = new THREE.SphereGeometry(31, 32, 32);
		// var cloud_material = new THREE.MeshBasicMaterial({
		// 	shininess   : 10,
		// 	map         : cloud_texture,
		// 	transparent : true,
		// 	opacity     : 0.8
		// });
		// cloud = new THREE.Mesh(cloud_geometry, cloud_material);
		// this.scene.add(cloud);

	}
	lglt2xyz(lng, lat, radius) {
		const phi = (180 + lng) * (Math.PI / 180)
		const theta = (90 - lat) * (Math.PI / 180)
		return {
			x: -radius * Math.sin(theta) * Math.cos(phi),
			y: radius * Math.cos(theta),
			z: radius * Math.sin(theta) * Math.sin(phi),
		}
	}
	//请求中国板块json
	axiosChinaJson() {
		let that = this;
		axios({
			type: "GET", //提交方式 
			url: "./static/china.json"
		}).then(res => {
			let arr = res.data;
			// 标记点组合
			//画中国板块的数据
			let chinaArr = []
			arr.features.forEach(ele => {
				ele.geometry.coordinates.forEach(ele2 => {
					let xArr = []
					ele2.forEach(ele3 => {
						ele3.forEach(markingItem => {
							// 获取标记点坐标
							var ballPos = this.getPosition(markingItem[0] + 90, markingItem[1], 30);
							let arrItem = [ballPos.x, ballPos.y, ballPos.z];
							xArr.push(arrItem)
						})
					})
					chinaArr.push(xArr)
				})
			})
			this.addRoadLine(chinaArr);
		})
	}
	//添加 平顺 直线曲线 效果
	addRoadLine(arr) {
		arr.forEach(
			(e, r) => {//map:this.spriteLightTexture
				//定义材质THREE.LineBasicMaterial . MeshBasicMaterial...都可以
				var material = new THREE.MeshBasicMaterial({
					color: '#ffe000', linewidth: 4, fog: true, transparent: true, //使用背景透明的png贴图，注意开启透明计算
					// opacity: 0.5,
				});
				// 空几何体，里面没有点的信息,不想BoxGeometry已经有一系列点，组成方形了。
				var geometry = new THREE.Geometry();
				// 给空白几何体添加点信息，这里写3个点，geometry会把这些点自动组合成线，面。
				e.forEach(ele => {
					geometry.vertices.push(new THREE.Vector3(ele[0], ele[1], ele[2]));
				})
				//线构造
				var line = new THREE.Line(geometry, material);
				// 加入到场景中
				this.scene.add(line);
			}
		)
	}
	// 经纬度转换函数
	getPosition(longitude, latitude, radius) {
		var lg = THREE.Math.degToRad(longitude);
		var lt = THREE.Math.degToRad(latitude);
		var temp = radius * Math.cos(lt);
		var x = temp * Math.sin(lg);
		var y = radius * Math.sin(lt);
		var z = temp * Math.cos(lg);
		return {
			x: x,
			y: y,
			z: z
		}
	}
	loadLL() {
		let obj = this.lglt2xyz(116, 39, 30);
		// this.createPointMesh2(obj, new THREE.TextureLoader().load("./static/img/wave.png"),'#31f919');
		
		this.createPointMesh2(obj, new THREE.TextureLoader().load("./static/img/wave.png"),'#ff0000');

		//  this.loadJlt(obj,new THREE.TextureLoader().load("./assets/q.png"))
	}
	createPointMesh(pos, texture) {
		var material = new THREE.MeshBasicMaterial({
			map: texture,
			transparent: true, //使用背景透明的png贴图，注意开启透明计算
			// side: THREE.DoubleSide, //双面可见
			depthWrite: false, //禁止写入深度缓冲区数据
		});
		var radius = 30;
		var planGeometry = new THREE.PlaneGeometry(0.7, 1)
		var mesh = new THREE.Mesh(planGeometry, material);
		var size = radius * 0.04;//矩形平面Mesh的尺寸
		mesh.scale.set(size, size, size);//设置mesh大小
		//设置mesh位置
		mesh.position.set(pos.x, pos.y, pos.z);
		// mesh在球面上的法线方向(球心和球面坐标构成的方向向量)
		var coordVec3 = new THREE.Vector3(pos.x, pos.y, pos.z).normalize();
		// mesh默认在XOY平面上，法线方向沿着z轴new THREE.Vector3(0, 0, 1)
		var meshNormal = new THREE.Vector3(0, 0, 1);
		// 四元数属性.quaternion表示mesh的角度状态
		//.setFromUnitVectors();计算两个向量之间构成的四元数值
		mesh.quaternion.setFromUnitVectors(meshNormal, coordVec3);
		return mesh;
	}
	//测试
	createPointMesh2(pos, texture,color) {
		var material = new THREE.MeshBasicMaterial({
			color,
			map: texture,
			transparent: true, //使用背景透明的png贴图，注意开启透明计算
			opacity: 1.0,
			side: THREE.DoubleSide, //双面可见
			depthWrite: false, //禁止写入深度缓冲区数据
		});
		var radius = 30;
		var planGeometry = new THREE.PlaneBufferGeometry(0.5, 0.5);
		var mesh = new THREE.Mesh(planGeometry, material);
		var size = radius * 0.04;//矩形平面Mesh的尺寸
		mesh.scale.set(size, size, size);//设置mesh大小
		mesh.size = size;
		//设置mesh位置
		mesh.position.set(pos.x, pos.y, pos.z);
		// mesh在球面上的法线方向(球心和球面坐标构成的方向向量)
		var coordVec3 = new THREE.Vector3(pos.x, pos.y, pos.z).normalize();
		// mesh默认在XOY平面上，法线方向沿着z轴new THREE.Vector3(0, 0, 1)
		var meshNormal = new THREE.Vector3(0, 0, 1);
		// 四元数属性.quaternion表示mesh的角度状态
		//.setFromUnitVectors();计算两个向量之间构成的四元数值
		mesh.quaternion.setFromUnitVectors(meshNormal, coordVec3);
		mesh._s = Math.random() * 1.0 + 1.0;
		this.meshArr.push(mesh);
		this.scene.add(mesh);


		//光柱
		var columnGeom = new THREE.CylinderGeometry(0.1, 0.1, 4, 32);
		var columnMaterial = new THREE.MeshBasicMaterial({
			//设置矩形网格模型的纹理贴图(光柱特效)
			// map: this.spriteLightTextureQ,
			// 双面显示
			side: THREE.DoubleSide,
			color,
			// 开启透明效果，否则颜色贴图map的透明不起作用
			transparent: true,
		});
		var columnMesh = new THREE.Mesh(columnGeom, columnMaterial);
		//设置mesh位置
		columnMesh.position.set(pos.x, pos.y, pos.z);
		this.scene.add(columnMesh);
		// 旋转
		var matrix = new THREE.Matrix4();
		matrix.makeRotationX(Math.PI / 2);
		matrix.setPosition(new THREE.Vector3(0, 0, 1));
		columnGeom.applyMatrix(matrix);
		columnMesh.lookAt(0, 0, 0);
		this.zcMeshArr.push(mesh,columnMesh)

	}
	loadJlt(pos, texture) {
		//创建精灵图
		var material = new THREE.SpriteMaterial({
			map: texture
		});
		var spriteTL = new THREE.Sprite(material);
		// spriteTL.scale.set(...scale[val.img]);

		spriteTL.position.set(pos.x, pos.y, pos.z - 0.5)
		this.scene.add(spriteTL);
		this.zcMeshArr.push(spriteTL)
	}
}