//引入three。js
import * as THREE from 'three'
//后期处理 物体周围发光
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer.js';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass.js';
import { OutlinePass } from 'three/examples/jsm/postprocessing/OutlinePass.js';
import { FXAAShader } from 'three/examples/jsm/shaders/FXAAShader.js';
//旋转控件
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { Message } from 'element-ui';
//各种加载器
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader'
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader'
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader.js';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { ColladaLoader } from 'three/examples/jsm/loaders/ColladaLoader.js';

import { CSS3DRenderer } from 'three/examples/jsm/renderers/CSS3DRenderer'
import { CSS3DObject } from 'three/examples/jsm/renderers/CSS3DRenderer';

import { GUI } from 'three/examples/jsm/libs/dat.gui.module.js'
import TWEEN from '@tweenjs/tween.js'

import { LineSegmentsGeometry } from 'three/examples/jsm/lines/LineSegmentsGeometry.js'
import { LineGeometry } from 'three/examples/jsm/lines/LineGeometry.js'
import { LineMaterial } from 'three/examples/jsm/lines/LineMaterial.js'
import { LineSegments2 } from 'three/examples/jsm/lines/LineSegments2.js'
import { Line2 } from 'three/examples/jsm/lines/Line2.js'
// import { Sky } from './Sky';
export default class threeApp {
  constructor(container, options) {
    //共享参数
    //dom节点
    this.dom = container;
    //场景
    this.scene = '';
    //相机
    this.camera = '';
    //渲染器
    this.renderer = '';
    //控制器
    this.controls = '';
    //动画
    this.frameId = '';
    //鼠标事件
    this.raycaster = new THREE.Raycaster();
    this.mouse = new THREE.Vector2();
    //可以点击 元素组
    this.clickArr = [];
    //控制特效的
    this.s = 0;
    this.p = 0;
    this.y = 0;
    //圈扩散的
    this.scanList = [];
    //用于画线的数据
    this.lineGroup = [];
    this.lineC = [];
    this.pointGroup = [];
    this.startHuaLine = false;
    //事件
    this.mouseDown = null;
  }
  //初始化
  init(options) {
    // 创建场景
    this.scene = new THREE.Scene();

    // this.scene.fog = new THREE.Fog(0xc8c8c8,4000,000);
    this.clock = new THREE.Clock();
    //创建相机
    this.camera = new THREE.PerspectiveCamera(45, this.dom.clientWidth / this.dom.clientHeight, 1, 1000);

    this.camera.lookAt(0, 0, 0);
    //创建渲染器
    this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    //设置渲染区域尺寸
    this.renderer.setSize(this.dom.clientWidth, this.dom.clientHeight);
    this.renderer.setClearColor(0xffffff, 0);
    this.renderer.setPixelRatio(window.devicePixelRatio);

    this.renderer.shadowMap.enabled = true;

    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    this.renderer.outputEncoding = THREE.sRGBEncoding;
    // 三维lable
    this.css3DRenderer = new CSS3DRenderer();
    this.css3DRenderer.domElement.style.position = 'absolute'
    this.css3DRenderer.domElement.style.top = 0;
    this.css3DRenderer.domElement.id = 'label3DRenderer';
    this.css3DRenderer.domElement.style.pointerEvents = "none"
    this.css3DRenderer.setSize(this.dom.clientWidth, this.dom.clientHeight);
    this.dom.appendChild(this.css3DRenderer.domElement);
    //设置背景颜色
    if (options.clearColor) this.renderer.setClearColor(options.clearColor);
    // 辅助
    if (options.axes) this.scene.add(new THREE.AxesHelper(100));// 坐标轴辅助红x 绿y 蓝z
    if (options.gridHelper) this.scene.add(new THREE.GridHelper(100000, 120, 0xffffff, 0xffffff));// 网格参考线
    //将渲染器 添加到 dom中
    this.dom.appendChild(this.renderer.domElement);
    //尺寸变化 从新渲染
    this.windowResize = this.onWindowResize.bind(this, this.dom);
    window.addEventListener('resize', this.windowResize);


    //背景
    // let skyTexture = (new THREE.TextureLoader)//环境贴图
    // .setPath("./static/model/")
    // .load("bg.jpg");
    // this.scene.background = skyTexture;
    // this.renderer.setClearAlpha(1);
    if (!options.clearColor) {
      // this.skyTexture1 = (new THREE.CubeTextureLoader) //环境贴图
      //     .setPath("./static/R/sky/")
      //     .load(["px.jpg", "nx.jpg", "py.jpg", "ny.jpg", "pz.jpg", "nz.jpg"]);
      //     this.skyTexture1.encoding = THREE.sRGBEncoding;
      //     this.scene.background = this.skyTexture1;
    }
    //  可应用阴影
    // var directionalLight = new THREE.DirectionalLight("#aaa"); //模拟远处类似太阳的光源
    // directionalLight.position.set(300, 300, 300).normalize();
    // this.scene.add(directionalLight);
    // //环境光 环境光会均匀的照亮场景中的所有物体。
    // var ambientLight = new THREE.AmbientLight( 0xffffff, 0.7 );
    // this.scene.add(ambientLight);
    // point light (upper left)
    var pointLight = new THREE.PointLight(0xffffff);
    pointLight.position.set(-400, 100, 150);
    this.scene.add(pointLight);

    // ambient light
    var ambientLight = new THREE.AmbientLight(0x222222);
    this.scene.add(ambientLight);
    //摩卡什么转换 用于转换坐标
    this.projection = d3.geoMercator().center([140.0, 40]).scale(800).translate([0, 0]);

    //
    options.initFun && options.initFun();
  }
  //跟随屏幕变化
  onWindowResize(container) {
    this.camera.aspect = container.clientWidth / container.clientHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(container.clientWidth, container.clientHeight);
    this.css3DRenderer.setSize(window.innerWidth, window.innerHeight);
  }
  //点击画线
  onHuaDocumentMouseDown() {
    this.addRoadLine([this.lineC], { number: 50, radius: 0.7 * 50 });
    this.lineGroup.push(JSON.parse(JSON.stringify(this.lineC)));
    this.lineC = [];
    this.pointGroup = [];
    console.log(JSON.stringify(this.lineGroup));
  }
  //画点
  huaPoint(arr) {
    var geometry = new THREE.DodecahedronGeometry(20, 3);
    var material = new THREE.MeshPhongMaterial({ color: 0x4080ff, dithering: true });
    var mesh = new THREE.Mesh(geometry, material); //网格模型对象Mesh
    mesh.position.set(...arr)
    mesh.castShadow = true;
    this.scene.add(mesh); //网格模型添加到场景中;
    return mesh
  }
  //键盘事件
  onkeydown(fn) {
    var event = window.event || arguments.callee.caller.arguments[0]
    if (event.keyCode === 32) {
      if (this.startHuaLine) {
        this.startHuaLine = false;
        Message.warning('暂停画线')
      } else {
        this.startHuaLine = true;
        Message.success('开启画线')
      }
    }
    if (event.keyCode === 27) {
      // //控制器
      console.log(this.camera.position.x, this.camera.position.y, this.camera.position.z, '相机')
      console.log(this.controls.target.x, this.controls.target.y, this.controls.target.z)
      this.scene.remove(this.pointGroup[this.pointGroup.length - 1]);
      this.pointGroup.splice(this.pointGroup.length - 1, 1);
      this.lineC.splice(this.lineC.length - 1, 1);
      Message.warning('撤回一步')
    }
    if (event.keyCode === 13) {
      this.onHuaDocumentMouseDown();
    }
    fn && fn(event.keyCode)
  }
  //加载键盘事件
  loadKeydown(fn) {
    //键盘事件
    this.keydown = this.onkeydown.bind(this, fn);
    window.addEventListener('keydown', this.keydown, false);
  }
  //点击事件
  onDocumentMouseDown(fn) {
    var event = window.event || arguments.callee.caller.arguments[0]
    this.mouse.x = (event.offsetX / window.innerWidth) * 2 - 1;
    this.mouse.y = - (event.offsetY / window.innerHeight) * 2 + 1;
    // 通过鼠标点的位置和当前相机的矩阵计算出raycaster
    this.raycaster.setFromCamera(this.mouse, this.camera);
    // 获取raycaster直线和所有模型相交的数组集合
    var intersects = this.raycaster.intersectObjects(this.clickArr);
    if (intersects.length > 0) {
      fn(intersects)
    }
  }
  //加载点击事件
  loadClick(fn) {
    //点击
    this.mouseDown = this.onDocumentMouseDown.bind(this, fn);
    window.addEventListener('mousedown', this.mouseDown, false);
  }
  //控制器
  initOrbitControls(fn) {
    this.controls = new OrbitControls(this.camera, this.renderer.domElement)
    this.controls.enableDamping = true // 使动画循环使用时阻尼或自转 意思是否有惯性
    this.controls.dampingFactor = 0.25; // 动态阻尼系数 就是鼠标拖拽旋转灵敏度
    this.controls.enableZoom = true // 是否可以缩放
    // this.controls.autoRotate = true // 开启自转
    this.controls.autoRotateSpeed = 0.3 //自传速度
    this.controls.maxDistance = 20000;
    this.controls.enablePan = true // 是否开启右键拖拽
    //旋转最大程度
    this.controls.maxPolarAngle = Math.PI / 2 - 0.2;
    fn && fn();
  }
  //创建模型
  model(option) {
    // var geometry = new THREE.BoxGeometry(w, h, t); //创建一个立方体几何对象Geometry
    var geometry = new THREE.CylinderBufferGeometry(...option.meshParams);
    var material = new THREE.MeshPhongMaterial({ color: 0x4080ff, dithering: true });
    var mesh = new THREE.Mesh(geometry, material); //网格模型对象Mesh
    mesh.position.set(...option.position)
    mesh.castShadow = true;
    this.scene.add(mesh); //网格模型添加到场景中;
    return mesh
  };
  // 经纬度 转 xyz
  lgltxyz(arr) {
    const [x, y] = this.projection(arr);
    return { x: x, y: -y, z: 4.01 }
  }
  //贝塞尔曲线 等 公共 纹理
  addTexture(fn) {
    //用于流动线
    this.spriteLightTexture = new THREE.TextureLoader().load("./static/R/red_line.png")
    this.spriteLightTexture.wrapS = this.spriteLightTexture.wrapT = THREE.RepeatWrapping;
    this.spriteLightTexture.repeat.set(1, 1);
    this.spriteLightTexture.needsUpdate = !0;

    this.spriteLightTexture2 = new THREE.TextureLoader().load("./static/R/green_line.png")
    this.spriteLightTexture2.wrapS = this.spriteLightTexture2.wrapT = THREE.RepeatWrapping;
    this.spriteLightTexture2.repeat.set(1, 1);
    this.spriteLightTexture2.needsUpdate = !0;

    //半球同名贴图
    this.spriteLightTextureQ = new THREE.TextureLoader().load('./static/R/2.png');
    this.spriteLightTextureQ.wrapS = THREE.RepeatWrapping;
    this.spriteLightTextureQ.wrapT = THREE.RepeatWrapping;
    this.spriteLightTextureQ.repeat.set(10, 10);
    this.spriteLightTextureQ.needsUpdate = !0;


    this.lightBarTexture = new THREE.TextureLoader().load("./static/R/lightray.png");
    this.lightBarParticlesTexture = new THREE.TextureLoader().load("./static/R/particles.png");
    this.lightBarParticlesTexture.wrapS = THREE.RepeatWrapping, this.lightBarParticlesTexture.wrapT = THREE.RepeatWrapping;

    fn && fn();
  }
  //添加 贝塞尔 飞线效果
  addFlyLine(arr) {
    !this.spriteLightTexture && this.addTexture();
    arr.forEach((e) => {
      //计算出中间点
      var r = e.from.clone().add(e.to.clone()).divideScalar(2);
      //自定义中间点的高度
      r.y = 8000;
      //创建贝塞尔曲线
      var t = new THREE.QuadraticBezierCurve3(e.from, r, e.to),
        o = t.getPoints(120), //将贝塞尔曲线转换为多个点
        a = [];
      o.forEach(function (e) {
        return a.push([e.x, e.y, e.z])
      });
      //创建纹理
      var n = new THREE.MeshBasicMaterial({ map: this.spriteLightTexture, side: THREE.BackSide, transparent: !0, color: e.color }),
        i = this.createAnimateLine({ material: n, type: "pipe", pointList: a, number: 100 * 5, radius: 5 * 5 });
      this.scene.add(i)
    })
  }
  //添加 平顺 直线曲线 效果
  addRoadLine(arr, option, group = '') {
    !this.spriteLightTexture && this.addTexture();

    arr.forEach(
      (e, r) => {
        var t = new THREE.MeshBasicMaterial({ map: option.color ? this.spriteLightTexture : this.spriteLightTexture2, side: THREE.BackSide, transparent: !0, depthTest: false }),
          o = this.createAnimateLine({ type: "pipe", pointList: e, material: t, number: option.number, radius: option.radius }); //制作飞线
        if (group) {
          group.add(o)
        } else {
          this.scene.add(o)
        }
      }
    )
  }
  //通用函数
  createAnimateLine(option) {
    let curve
    if (option.kind === 'sphere') { // 由两点之间连线成贝塞尔曲线
      const sphereHeightPointsArgs = option.sphereHeightPointsArgs
      const pointList = app.getSphereHeightPoints(...sphereHeightPointsArgs) // v0,v3,n1,n2,p0
      curve = new THREE.CubicBezierCurve3(sphereHeightPointsArgs[0], pointList[0], pointList[1], sphereHeightPointsArgs[1])
    } else { // 由多个点数组构成的曲线 通常用于道路
      const l = []
      option.pointList.forEach(e => l.push(new THREE.Vector3(e[0], e[1], e[2])))
      curve = new THREE.CatmullRomCurve3(l) // 曲线路径
    }
    if (option.type === 'pipe') { // 使用管道线
      // 管道体
      const tubeGeometry = new THREE.TubeGeometry(curve, option.number || 50, option.radius || 1, option.radialSegments)
      return new THREE.Mesh(tubeGeometry, option.material)
    } else { // 使用 meshLine
      if (!MeshLine || !MeshLineMaterial) console.error('you need import MeshLine & MeshLineMaterial!')
      else {
        const geo = new THREE.Geometry()
        geo.vertices = curve.getPoints(option.number || 50)
        const meshLine = new MeshLine()
        meshLine.setGeometry(geo)
        return new THREE.Mesh(meshLine.geometry, option.material)
      }
    }
  }
  //渲染 动画
  render(fn) {
    this.renderer.render(this.scene, this.camera);
    this.frameId = requestAnimationFrame(() => {
      this.render(fn);
      fn && fn();
      //改变贴图位置实现特效
      this.spriteLightTexture && (this.spriteLightTexture.offset.x -= .01);
      this.spriteLightTexture2 && (this.spriteLightTexture2.offset.x -= .01);
      //半球特效
      this.ShaderBar4 && (this.ShaderBar4.uniforms.time.value -= 0.005);
      //动态墙
      this.ShaderBar5 && (this.ShaderBar5.uniforms.time.value += 0.01);
      //椎体
      this.cone && this.cone.rotateY(.01);
      //旋转加喷花
      this.lightBarParticlesTexture && (this.lightBarParticlesTexture.offset.y += .005);
      if (this.barParticlesList) {
        this.y += .03,
          this.barParticlesList.forEach(
            function (e) {
              return "barParticle" === e.name ? e.rotateY(.05) : ""
            }
          );
        this.scaleYList.forEach((e) => {
          return e.scale.set(1, 1.5 * (Math.sin(this.y) + 1) + 1, 1)
        })
        this.rotateYList.forEach(
          function (e) {
            return "circleZ" === e.name ? e.rotateZ(.05) : e.rotateY(.01)
          }
        )
      }
      //旋转
      this.circle && this.circle.rotateZ(.05);
      // console.log(this.camera,'camera')
      //控制圈扩散
      if (this.scanList.length > 0) this.scananimate();
      TWEEN && TWEEN.update();
      this.controls && this.controls.update();
      this.css3DRenderer && this.css3DRenderer.render(this.scene, this.camera);
    });
  }
  //四种加载器
  loadFbx(url, fn) {
    var loader = new FBXLoader(); //创建一个FBX加载器
    loader.load(url, (obj) => {
      fn(obj)
    })
  }
  loadDea(path, fn) {
    let loader = new ColladaLoader();
    loader.load(path, (collada) => {
      var avatar = collada.scene.children[0];
      fn(avatar)
    });
  }
  loadGltf(path, fn) {
    var loader = new GLTFLoader();
    loader.load(path, (gltf) => {
      fn(gltf.scene)
    }, undefined, function (e) {
      console.error(e);
    });
  }
  loadObj(path, mtl, obj, fn) {
    let self = this
    new MTLLoader()
      .setPath(path)
      .load(mtl, (materials) => {
        materials.preload();
        new OBJLoader()
          .setMaterials(materials)
          .setPath(path)
          .load(obj,
            (object) => {
              fn(object)
            });
      });
  }
  //销毁
  destroy(fn) {

    this.frameId && cancelAnimationFrame(this.frameId); //销毁requestAnimationFrame
    window.removeEventListener('mousedown', this.mouseDown, false)
    window.removeEventListener('keydown', this.keydown, false);
    window.removeEventListener('resize', this.windowResize, false);
    this.renderer.forceContextLoss() //销毁context
    this.scene.dispose()
    this.controls.dispose()
    this.renderer = null
    this.scene = null
    this.camera = null;
    fn && fn()
  }
  createText(text, position) {
    var shapes = this.font.generateShapes(text, 1);

    var geometry = new THREE.ShapeBufferGeometry(shapes);

    var material = new THREE.MeshBasicMaterial();

    var textMesh = new THREE.Mesh(geometry, material);
    textMesh.position.set(position.x, position.y, position.z);

    this.scene.add(textMesh);
  }
  //画线  宽度不可改变
  xuLine(arr) {
    var lineGeometry = new THREE.Geometry();//生成几何体
    arr.forEach(val => {
      lineGeometry.vertices.push(new THREE.Vector3(...val));
    })


    var line = new THREE.Line(lineGeometry, new THREE.LineDashedMaterial({
      color: '#fc5531',//线段的颜色
      linewidth: 100,
      scale: 10,
      dashSize: 0.5,//短划线的大小
      gapSize: 0.5//短划线之间的距离
    }));
    // line.computeLineDistances();//不可或缺的，若无，则线段不能显示为虚线
    this.scene.add(line);
  }
  //画线
  drawLine(arr, colorArr) {
    var arr2 = [].concat.apply([], arr);
    var geometry = new LineGeometry();
    // 顶点坐标构成的数组pointArr
    // var pointArr = [-100,0,0,
    //                 -100,100,0,
    //                 0,0,0,
    //                 100,100,0,
    //                 100,0,0,]
    // 几何体传入顶点坐标
    geometry.setPositions(arr2);
    // 设定每个顶点对应的颜色值
    geometry.setColors(colorArr);
    // 自定义的材质
    var material = new LineMaterial({
      // color: 0xdd2222,
      // 线宽度
      linewidth: 5,
      // 注释color设置，启用顶点颜色渲染
      vertexColors: THREE.VertexColors
    });
    // 把渲染窗口尺寸分辨率传值给材质LineMaterial的resolution属性
    // resolution属性值会在着色器代码中参与计算
    material.resolution.set(window.innerWidth, window.innerHeight);
    var line = new Line2(geometry, material);
    this.scene.add(line)
  }
  //地板
  loadFloor(arr) {
    let planeTexture = new THREE.TextureLoader().load("./static/R/floor-1.png");

    planeTexture.wrapS = planeTexture.wrapT = THREE.RepeatWrapping, planeTexture.repeat.set(20, 20);
    let a = new THREE.MeshBasicMaterial({
      // envMap: this.skyTexture1,
      color: "#000",
      fog: true
      // dithering: true,
      // opacity:0.8 ,
      // side: THREE.DoubleSide,
      //  transparent: true,
    });
    let geometry = new THREE.PlaneBufferGeometry(80000, 80000);
    let mesh = new THREE.Mesh(geometry, a);
    mesh.position.set(...arr);
    mesh.rotation.x = - Math.PI * 0.5;
    // mesh.receiveShadow = true;
    this.scene.add(mesh);
    this.clickArr.push(mesh)
  }
  //发光蓝色shader材质
  loadShader(fn) {
    var shader = {
      vertexShader: `
  varying vec2 vUv;
  varying vec3 fNormal;
  void main() {
    vUv = uv;
    fNormal=normal;
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
  } `,
      fragmentShader: ` 
    // 根据片元的高度来渐变
    varying vec2 vUv;
    uniform sampler2D texture1;
    varying vec3 fNormal;
    uniform sampler2D texture2;
    void main(){
      vec3 tempNomal= normalize(fNormal);
      float power=step(0.95,abs(tempNomal.y));
      if(power<0.95){
        gl_FragColor = mix(texture2D(texture1, vUv),vec4(gl_FragColor.r,gl_FragColor.g,gl_FragColor.b,0.0),0.2);  //再混合材质
      }else{
        gl_FragColor = mix(texture2D(texture2, vUv),vec4(gl_FragColor.r,gl_FragColor.g,gl_FragColor.b,0.0),0.2);  //再混合材质
      }    
    } `
    }
    this.ShaderBar = {
      uniforms: {
        texture1: {
          value: new THREE.TextureLoader().load("./static/R/9.png")
        },
        texture2: {
          value: new THREE.TextureLoader().load("./static/R/5.png")
        }
      },
      vertexShader: shader.vertexShader,
      fragmentShader: shader.fragmentShader
    };

    //dao ying shader
    var shaderDy = {
      vertexShader: `
  varying vec2 vUv;
  void main() {
    vUv = uv;
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position.x,position.y*-1.0,position.z, 1.0);
  } `,
      fragmentShader: ` 
    // 根据片元的高度来渐变
    varying vec2 vUv;
    uniform sampler2D texture1;
    void main(){
        gl_FragColor = mix(texture2D(texture1, vUv),vec4(gl_FragColor.r,gl_FragColor.g,gl_FragColor.b,0.3),0.6);  //再混合材质
   
    } `
    }
    this.ShaderBarDy = {
      uniforms: {
        texture1: {
          value: new THREE.TextureLoader().load("./static/R/10.png")
        }
      },
      vertexShader: shaderDy.vertexShader,
      fragmentShader: shaderDy.fragmentShader
    };
    fn && fn();
  }
  loadvNormal(option) {
    var shader = {
      vertexShader: `
      varying vec3 vNormal;
      varying vec3 vPositionNormal;
      varying vec2 vUv;
      void main() 
      {
          vUv = uv;
          vNormal = normalize( normalMatrix * normal ); // 转换到视图空间
          vPositionNormal = normalize(( modelViewMatrix * vec4(position, 1.0) ).xyz);
          gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
      }`,
      fragmentShader: ` uniform vec3 glowColor;
                      uniform float bias;
                      uniform float power;
                      uniform float scale;
                      varying vec3 vNormal;
                      varying vec3 vPositionNormal;
                      uniform sampler2D textureMap;
                      uniform vec2 repeat;
                      varying vec2 vUv;
                      uniform float time;
                      void main() 
                      {
                          float a = pow( bias + scale * abs(dot(vNormal, vPositionNormal)), power );
                          //*(vec2(1.0,time))
                          // vec4 mapColor=texture2D( textureMap, vUv*repeat);
                          vec4 colora = texture2D(textureMap,vec2(vUv.x,fract(vUv.y-time)));
                          gl_FragColor = vec4( glowColor*colora.rgb, a );
                      }`
    };
    this.ShaderBar4 = {
      uniforms: {
        scale: { type: "f", value: -1.0 },
        bias: { type: "f", value: 1.0 },
        power: { type: "f", value: 1.3 },
        glowColor: { type: "c", value: new THREE.Color(0x00ffff) },
        textureMap: {
          value: undefined
        },
        repeat: {
          type: "v2",
          value: new THREE.Vector2(30.0, 15.0)
        },
        time: {
          value: 0.0
        }
      },
      vertexShader: shader.vertexShader,
      fragmentShader: shader.fragmentShader
    };


    this.ShaderBar4.uniforms.textureMap.value = this.spriteLightTextureQ;

    let geo = new THREE.SphereGeometry(...option.meshParams);
    let material = new THREE.ShaderMaterial({
      uniforms: this.ShaderBar4.uniforms,
      vertexShader: this.ShaderBar4.vertexShader,
      fragmentShader: this.ShaderBar4.fragmentShader,
      //side: DoubleSide,
      //blending:AdditiveBlending,
      transparent: true,
      depthWrite: false

    });
    var mesh = new THREE.Mesh(geo, material);
    mesh.position.set(...option.position);
    this.scene.add(mesh);
    return mesh
  }
  //模型蓝线材质
  initLineMaterial() {
    this.lineMaterial = new THREE.LineBasicMaterial({
      // 线的颜色
      color: "#0033ff",
      transparent: true,
      linewidth: 5,
      opacity: 1.0,
      //depthTest: true,
    });
    //解决z-flighting
    this.lineMaterial.polygonOffset = true;
    this.lineMaterial.depthTest = true;
    this.lineMaterial.polygonOffsetFactor = 1;
    this.lineMaterial.polygonOffsetUnits = 1.0;
  }
  //
  addCircleBottom() {
    this.circle = new THREE.Mesh(
      new THREE.CircleGeometry(200, 32),
      new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load("./static/R/circular.png"), color: "#ffec0c", side: THREE.DoubleSide, transparent: !0 })
    );

    this.circle.position.set(-1904, -60, 5950);
    this.circle.rotateX(Math.PI / 2);
    this.circle.scale.set(14, 14, 14)
    this.scene.add(this.circle);
    return this.circle
  }
  //圈扩散的动画
  addScan(Parr) {
    var e = new THREE.CylinderGeometry(1000, 1000, 500, 84),
      r = new THREE.Mesh(
        e, [
        new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load("./static/R/wall.png"), side: THREE.DoubleSide, transparent: !0 }),
        new THREE.MeshBasicMaterial({ transparent: !0, opacity: 0, side: THREE.DoubleSide }),
        new THREE.MeshBasicMaterial({ transparent: !0, opacity: 0, side: THREE.DoubleSide })
      ]
      );
    r.position.set(...Parr);
    r.renderOrder = 1e3;
    this.scanList.push(r);
    this.scene.add(r)
  }
  //圈扩散的动画
  scananimate() {
    this.s > 5 && (this.s = 0, this.p = 1);
    this.s += .01;
    this.p -= .002;
    this.scanList.forEach(
      (e) => {
        e.scale.set(1 + this.s, 1, 1 + this.s);
        e.material[0].opacity = this.p
      })
  }
  add3dText(text, width, Parr, isR) {
    let html = `
      <div class="box2" id="box2">
          <div class="ys-block">
              <div style='width:${width}px' class="ys-con">
                  ${text}
              </div>
          </div>
      </div>
      `
    let bloodDiv = document.createElement('div');
    bloodDiv.innerHTML = html
    const label3d = new CSS3DObject(bloodDiv)
    // @ts-ignore
    label3d.position.set(...Parr);
    label3d.scale.set(100 / 5, 130 / 5, 100 / 5);
    // this.setGUI(label3d)
    // label3d.rotateZ(Math.PI / 2);
    // isR && label3d.rotateX(-Math.PI / 2);
    this.scene.add(label3d);
    return label3d
  }
  add3dTextTwo(text, width, Parr, isR) {
    let html = `
      <div class="textTwo">
      <div style='width:${width}px'>${text} </div>
      </div>
      `
    let bloodDiv = document.createElement('div');
    bloodDiv.innerHTML = html
    const label3d = new CSS3DObject(bloodDiv);
    label3d.position.set(...Parr);
    label3d.scale.set(10, 10, 10);
    isR && label3d.rotateX(-Math.PI / 2);
    this.scene.add(label3d);
    return label3d
  }
  add3dWaringImg(Parr) {
    let html = `
      <div class="waringImg">
      <div> </div>
      </div>
      `
    let bloodDiv = document.createElement('div');
    bloodDiv.innerHTML = html;
    bloodDiv.style.pointerEvents = "none"
    const label3d = new CSS3DObject(bloodDiv)
    label3d.position.set(...Parr);
    label3d.scale.set(4, 4, 4);
    this.scene.add(label3d);
    return label3d
  }
  addLightBar(e, r, t) {
    let LightGroup = new THREE.Group();
    var renderOrder = 1;
    this.barParticlesList = [];
    this.rotateYList = [];
    this.scaleYList = [];
    if (0 === r) {
      var o = 200,
        a = new THREE.Mesh(new THREE.PlaneGeometry(30, o),
          new THREE.MeshBasicMaterial({ map: this.lightBarTexture, color: t, transparent: !0, side: THREE.DoubleSide })),
        n = a.clone();
      n.rotateY(-Math.PI / 2);
      var i = new THREE.Mesh(
        new THREE.PlaneGeometry(30, o),
        new THREE.MeshBasicMaterial({ map: this.lightBarParticlesTexture, color: "#fff", transparent: !0, side: THREE.DoubleSide }));
      i.rotateY(Math.PI / 4);
      var s = i.clone();
      s.rotateY(-Math.PI / 2),
        a.add(n), i.add(s),
        a.position.set(e[0], e[1] + 100, e[2]),
        i.position.set(e[0], e[1] + 100, e[2]),
        a.rotateX(Math.PI),
        i.rotateX(Math.PI),
        a.renderOrder = renderOrder++,
        n.renderOrder = renderOrder++,
        i.renderOrder = renderOrder++,
        s.renderOrder = renderOrder++,
        this.barParticlesList.push(a, i),
        i.name = "barParticle",
        LightGroup.add(a, i);
      var l = new THREE.CircleGeometry(200, 100),
        d = new THREE.Mesh(
          l,
          new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load("./static/R/circle.png"), color: t, side: THREE.DoubleSide, transparent: !0 })
        );
      d.position.set(e[0], e[1] + 5, e[2]),
        d.name = "circleZ",
        d.rotateX(Math.PI / 2),
        d.renderOrder = renderOrder++;
      var c = new THREE.Mesh(new THREE.RingBufferGeometry(200, 205, 100), new THREE.MeshBasicMaterial({ color: t, side: THREE.DoubleSide })); c.position.set(e[0], e[1] + 5, e[2]), c.rotateX(Math.PI / 2), LightGroup.add(d, c), this.rotateYList.push(d), this.scaleYList.push(a, i); var p = new THREE.Mesh(new THREE.CylinderGeometry(20, 1, 10, 6), new THREE.MeshBasicMaterial({ color: t })); p.position.set(e[0], e[1] + .5, e[2]), p.renderOrder = renderOrder++, LightGroup.add(p);
      this.scene.add(LightGroup);
      return LightGroup
    }
  }
  //椎体
  addZt(option) {
    var r = new THREE.ConeGeometry(150, 300, 4),
      t = new THREE.MeshBasicMaterial({ color: 16776960 });
    this.cone = new THREE.Mesh(r, t),
      this.cone.position.set(...option.position),
      this.cone.rotateX(Math.PI);
    this.scene.add(this.cone);
  }
  //动态墙
  loadDTQ(option) {
    var shader = {
      vertexShader: `
      varying vec2 vUv;
                    varying vec3 fNormal;
                    varying vec3 vPosition;
                    void main()
                    {
                        vUv = uv;
                        fNormal=normal;
                        vPosition=position;
                        vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
                        gl_Position = projectionMatrix * mvPosition;
                    }`,
      fragmentShader: `  uniform float time;
      varying vec2 vUv;
      uniform sampler2D colorTexture;
      uniform sampler2D colorTexture1;
      varying vec3 fNormal;
      varying vec3 vPosition;
      void main( void ) {
          vec2 position = vUv;
          vec3 tempNomal= normalize(fNormal);
          float power=step(0.95,abs(tempNomal.y));
          // gl_FragColor = mix(texture2D(texture1, vUv),vec4(gl_FragColor.r,gl_FragColor.g,gl_FragColor.b,0.0),opacityss)
          vec4 colorb=texture2D(colorTexture1,position.xy);
          vec4 colora = texture2D(colorTexture,vec2(vUv.x,fract(vUv.y-time))); 
          if(power>0.95){
              gl_FragColor =colorb;
          }else{
              gl_FragColor =colorb+colorb*colora;      
          }         
      }`
    };
    this.ShaderBar5 = {
      uniforms: {
        colorTexture1: {
          value: new THREE.TextureLoader().load("./static/R/123.png")
        },
        colorTexture: {
          value: undefined
        },
        time: {
          value: 0.0
        }
      },
      vertexShader: shader.vertexShader,
      fragmentShader: shader.fragmentShader
    };
    let tx = new THREE.TextureLoader().load('./static/R/999.png');
    // tx.wrapS = THREE.RepeatWrapping;
    // tx.wrapT=THREE.RepeatWrapping;
    tx.wrapS = tx.wrapT = THREE.RepeatWrapping, tx.repeat.set(20, 20);
    this.ShaderBar5.uniforms.colorTexture.value = tx;

    var geometry = new THREE.BoxGeometry(...option.meshParams);
    var l = new THREE.ShaderMaterial({
      uniforms: this.ShaderBar5.uniforms,
      vertexShader: this.ShaderBar5.vertexShader,
      fragmentShader: this.ShaderBar5.fragmentShader,
      blending: THREE.AdditiveBlending,
      transparent: true,
      depthTest: false,
      side: THREE.DoubleSide
    })
    var mesh = new THREE.Mesh(geometry, [
      l,
      l,
      new THREE.MeshBasicMaterial({ transparent: true, opacity: 0, side: THREE.DoubleSide, depthTest: false }),
      new THREE.MeshBasicMaterial({ transparent: true, opacity: 0, side: THREE.DoubleSide, depthTest: false }), l, l,

    ]);
    mesh.position.set(...option.position);

    this.scene.add(mesh)
    return mesh
  }
  flyTo(option) {
    option.position = option.position || [] // 相机新的位置
    option.controls = option.controls || [] // 控制器新的中心点位置(围绕此点旋转等)
    option.duration = option.duration || 1000 // 飞行时间
    option.easing = option.easing || TWEEN.Easing.Linear.None
    // TWEEN.removeAll()
    const curPosition = this.camera.position
    const controlsTar = this.controls.target
    const tween = new TWEEN.Tween({
      x1: curPosition.x, // 相机当前位置x
      y1: curPosition.y, // 相机当前位置y
      z1: curPosition.z, // 相机当前位置z
      x2: controlsTar.x, // 控制当前的中心点x
      y2: controlsTar.y, // 控制当前的中心点y
      z2: controlsTar.z // 控制当前的中心点z
    }).to({
      x1: option.position[0], // 新的相机位置x
      y1: option.position[1], // 新的相机位置y
      z1: option.position[2], // 新的相机位置z
      x2: option.controls[0], // 新的控制中心点位置x
      y2: option.controls[1], // 新的控制中心点位置x
      z2: option.controls[2]  // 新的控制中心点位置x
    }, option.duration).easing(TWEEN.Easing.Linear.None) // TWEEN.Easing.Cubic.InOut //匀速
    tween.onUpdate(() => {
      this.controls.enabled = false
      this.camera.position.set(tween._object.x1, tween._object.y1, tween._object.z1)
      this.controls.target.set(tween._object.x2, tween._object.y2, tween._object.z2)
      this.controls.update()
      if (option.update instanceof Function) { option.update(tween) }
    })
    tween.onStart(() => {
      this.controls.enabled = false
      if (option.start instanceof Function) { option.start() }
    })
    tween.onComplete(() => {
      this.controls.enabled = true
      if (option.done instanceof Function) { option.done() }
    })
    tween.onStop(() => option.stop instanceof Function ? option.stop() : '')
    tween.start()
    TWEEN.add(tween)
    return tween
  }
  
  loadGui(moedl) {
    let gui = new GUI();
    const params = {
      scaleX: moedl.scale.x,
      scaleY: moedl.scale.y,
      scaleZ: moedl.scale.z,
      positionX: moedl.position.x,
      positionY: moedl.position.y,
      positionZ: moedl.position.z,
      rotationX: moedl.rotation.x,
      rotationY: moedl.rotation.y,
      rotationZ: moedl.rotation.z
    };
    gui.add(params, 'scaleX', 0, 50).onChange((val) => {

      moedl.scale.x = val

    });
    gui.add(params, 'scaleY', 0, 50).onChange((val) => {

      moedl.scale.y = val

    });
    gui.add(params, 'scaleZ', 0, 50).onChange((val) => {

      moedl.scale.z = val

    });
    gui.add(params, 'positionX', -90000, 90000).onChange((val) => {

      moedl.position.x = val

    });
    gui.add(params, 'positionY', -90000, 90000).onChange((val) => {

      moedl.position.y = val

    });
    gui.add(params, 'positionZ', -90000, 120000).onChange((val) => {

      moedl.position.z = val

    });
    gui.add(params, 'rotationX', 0, 4).onChange((val) => {

      moedl.rotation.x = val

    });
    gui.add(params, 'rotationY', 0, 4).onChange((val) => {

      moedl.rotation.y = val

    });
    gui.add(params, 'rotationZ', 0, 4).onChange((val) => {

      moedl.rotation.z = val

    });
    this.scene.add(gui)
  }
  //三个点 求角度函数
  threePoints(option) {
    let A = option.point1;
    let B = option.point2;
    let C = option.point3;

    var AB = Math.sqrt(Math.pow(A.X - B.X, 2) + Math.pow(A.Y - B.Y, 2));
    var AC = Math.sqrt(Math.pow(A.X - C.X, 2) + Math.pow(A.Y - C.Y, 2));
    var BC = Math.sqrt(Math.pow(B.X - C.X, 2) + Math.pow(B.Y - C.Y, 2));
    var cosA = (
      Math.pow(AB, 2) + Math.pow(AC, 2) - Math.pow(BC, 2)
    ) / (
        2 * AB * AC
      );
    var angleA = Math.round(Math.acos(cosA) * 180 / Math.PI);
    return angleA;
  }
}