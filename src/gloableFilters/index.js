// 全局过滤器
export default {
    // 时间戳转换日期格式
    dateFilter(timestamp) {
        var date = new Date(timestamp);
        var YY = date.getFullYear() + '-';
        var MM = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
        var DD = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate());
        var hh = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
        var mm = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
        var ss = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
        return YY + MM + DD + " " + hh + mm + ss;
    },
    dateFilter1(timestamp) {
        var date = new Date(timestamp);
        var YY = date.getFullYear() + '-';
        var MM = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
        var DD = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate());
        return YY + MM + DD;
    },
    dateFilter2(timestamp) {
        var date = new Date(timestamp);
        var hh = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
        var mm = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
        var ss = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
        return hh + mm + ss;
    },
    dateFilter3(timestamp) {
        var date = new Date(timestamp);
        var hh = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
        var mm = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
        return hh + mm;
    },
    getWeek(timestamp) {
        var week;
        var date = new Date(timestamp);
        if (date.getDay() == 0) week = "星期日"
        if (date.getDay() == 1) week = "星期一"
        if (date.getDay() == 2) week = "星期二"
        if (date.getDay() == 3) week = "星期三"
        if (date.getDay() == 4) week = "星期四"
        if (date.getDay() == 5) week = "星期五"
        if (date.getDay() == 6) week = "星期六"
        return week;
    },
    realtimeH1(timestamp) {
        var date = new Date(timestamp);
        var hh = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours());
        var h = parseInt((hh % 100) / 10);
        return h;
    },
    realtimeH2(timestamp) {
        var date = new Date(timestamp);
        var hh = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours());
        var h = parseInt(hh % 10);
        return h;
    },
    realtimem1(timestamp) {
        var date = new Date(timestamp);
        var mm = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
        var m = parseInt((mm % 100) / 10);
        return m;
    },
    realtimem2(timestamp) {
        var date = new Date(timestamp);
        var mm = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
        var m = parseInt(mm % 10);
        return m;
    },
    realtimes1(timestamp) {
        var date = new Date(timestamp);
        var ss = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
        var s = parseInt((ss % 100) / 10);
        return s;
    },
    realtimes2(timestamp) {
        var date = new Date(timestamp);
        var ss = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
        var s = parseInt(ss % 10);
        return s;
    },
}