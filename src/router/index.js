import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home.vue'
import popup from '../views/popup/index.vue'
import ships from '../views/ships/index.vue'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'popup',
    component: popup
}, {
    path: '/ships',
    name: 'ships',
    component: ships
}, ]

const router = new VueRouter({
    routes
})

export default router