import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false
    // import 'default-passive-events'
    // 引入全局组件
    // import components from "./components/index";
    // Vue.use(components);

//element
import 'element-ui/lib/theme-chalk/index.css';
import Element from 'element-ui';
Vue.use(Element, { size: 'small', zIndex: 3000 });
import './style/element-variables.scss'

// 引入全局过滤器
import filters from "@/gloableFilters";
for (const key in filters) {
    Vue.filter(key, filters[key]);
}
//引入全局方法
import globalFunction from "./lib/common";
Vue.prototype.$gf = globalFunction;

// 引入全局组件
import components from "./components/index";
Vue.use(components);

//拖拽
import '@/lib/directives.js'

//初始化样式
import './style/index.scss'
// 引入全局echarts
import * as echarts from 'echarts'
Vue.prototype.$echarts = echarts

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')