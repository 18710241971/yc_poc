export default {
  setSs(name, val) {
    let va = JSON.stringify(val);
    window.sessionStorage.setItem(name, va);
  },
  getSs(name) {
    let ss = window.sessionStorage.getItem(name)
    let val = JSON.parse(ss);
    return val
  },
  downloadFile(blob) {
    const reader = new FileReader();
    reader.readAsDataURL(blob);
    reader.onload = (e) => {
      const a = document.createElement('a');
      // a.download = `驾驶员.xlsx`;
      a.href = e.target.result;
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    };
  },
  initWebSocket(path, fn) {
    let socket = "";
    let open = () => {
      socket.send("game2");
      console.log("连接cg");
    };
    let error = () => {
      console.log("连接错误");
    };
    let getMessage = fn;
    let close = () => {
      console.log("socket已经关闭");
    };
    if (typeof WebSocket === "undefined") {
      alert("您的浏览器不支持socket");
    } else {

      // 实例化socket
      socket = new WebSocket(path);
      // 监听socket连接
      socket.onopen = open;
      // 监听socket错误信息
      socket.onerror = error;
      // 监听socket消息
      socket.onmessage = getMessage;
      socket.onclose = close;
      return socket;
    }
  },
  dateFilter(timestamp) {
    var date = new Date(timestamp);
    var YY = date.getFullYear() + '-';
    var MM = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var DD = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate());
    var hh = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
    var mm = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
    var ss = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    return YY + MM + DD + " " + hh + mm + ss;
  },
  dateFilterHMS() {
    var date = new Date();
    // var YY = date.getFullYear() + '-';
    // var MM = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    // var DD = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate());
    var hh = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
    var mm = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
    var ss = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    return hh + mm + ss;
  },
};