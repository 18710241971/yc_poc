import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 场景切换
    sceneChange: 1,
    //大屏实时数据
    dpSsData: {
      dpsizeOnDisk: '12',
      dphistoryData: {
        distance_sum: 0,
        height_mean: 0,
        speed_mean: 0,
        track_sum: 0,
      },
      dpTealTimeData: {
        meanHeight: 0,
        averageVelocity: 0
      },
      dpsumHexByTime: [],
      dpsumPassby: {},
      dpsumWarnByTime: [],
      dpsumWarnRealTime: 0,
      dptakeOffRecent: [],
      dpwarnRecent: []
    },
    //飞机总数
    flyCount: 0,
    //筛选飞机总数
    flySyCount: 0,
    //飞机筛选条件
    flyTj: {
      speed: [0, 306],
      height: [0, 120500],
      checkList: []
    },
    // 场景2数据
    sceneData: {
      stepOne: {},
      stepTwo: {},
      stepThree: {},
      stepFour: {},
    },
    //结束1  回放2
    isJsHf: { val: 0 },
    //场景动态数据
    scenedtData: {
      stepOne: {
        lon: 0,
        lat: 0,
        h: 0,
        sd: 0,
        hx: 0,
        olon: 0,
        olat: 0
      },
      stepTwo: {
        lon: 0,
        lat: 0,
        h: 0,
        sd: 0,
        hx: 0,
        olon: 0,
        olat: 0
      }
    },
    //弹框
    cautionShow: {
      val: 0,
      mess: ''
    },
    //监听是否同意
    isCautionShow: false,
    //场景3图片
    s3Img: 1
  },
  mutations: {
    resetflyTj(state, data) {
      if (data = 'speed') {
        state.flyTj[data] = [0, 306]
      }
      if (data = 'height') {
        state.flyTj[data] = [0, 120500]
      }
      if (data = 'checkList') {
        state.flyTj[data] = []
      }
    },
    sets3Img(state, data) {
      state.s3Img = data;
    },
    setisJsHf(state, data) {
      state.isJsHf = data;
    },
    setflySyCoun(state, data) {
      state.flySyCount = data;
    },
    changeCaution(state, data) {
      state.cautionShow.val = data.val;
      state.cautionShow.mess = data.mess;
    },
    setisCautionShow(state) {
      state.isCautionShow = !state.isCautionShow;
    },
    setSceneChange(state, data) {
      state.sceneChange = data;
    },
    setdpSsData(state, data) {
      state.dpSsData = data;
    },
    setflyCount(state, data) {
      state.flyCount = data;
    },
    setSceneData(state, data) {
      switch (data.object.stepId) {
        case '1':
          state.sceneData.stepOne = data.object;
          break;
        case '2':
          state.sceneData.stepTwo = data.object;
          break;
        case '3':
          state.sceneData.stepThree = data.object;
          break;
        case '4':
          state.sceneData.stepFour = data.object;
          break;
      }

    },
    setscenedtData(state, data) {
      switch (data.stepId) {
        case '1':
          state.scenedtData.stepOne = data;
          break;
        case '2':
          state.scenedtData.stepTwo = data;
          break;
      }

    },
  },
  actions: {

  },
  modules: {}
})