// 找到指定文件下所有后缀为vue的注册成全局组件
const components = require.context("./", false, /\.vue$/);

export default (Vue) => {
    components.keys().map((item) => {
        Vue.component(item.split(".")[1].split("/")[1], components(item).default);
    });
};