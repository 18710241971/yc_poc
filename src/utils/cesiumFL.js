//多圈涟漪
import CircleWaveMaterialProperty from './CesiumCircleWaveMaterial'
import CircleWaveMaterialPropertyP from './circlewave'
export default {
  //添加多圈涟漪
  /**
   * 对象 包含位置
   * @param {*} val 
   */
  addRipples(val) {
    let en = viewer.entities.add({
      position: Cesium.Cartesian3.fromDegrees(val.lon, val.lat),
      ellipse: {
        semiMinorAxis: val.fw,
        semiMajorAxis: val.fw,
        // height : 2000,
        material: new CircleWaveMaterialProperty({
          duration: 2e3,
          gradient: 0,
          color: new Cesium.Color(1.0, 0.0, 0.0, 0.8),
          count: 3
        })
      }
    });
    return en
  },
  //相机飞行视角改变
  /**
   * 
   * @param {duration} 飞行时间 
   */
  cameraFlying(params) {
    if (!params.duration) {
      // 普通相机视角
      viewer.camera.setView({
        destination: Cesium.Cartesian3.fromDegrees(params.lon, params.lat, params.h),
        orientation: {
          heading: params.heading || 0.0,
          pitch: params.pitch || 0.0,
          roll: params.roll || 0.0,
        }
      });
    } else {
      // 相机飞行动画
      viewer.camera.flyTo({
        destination: Cesium.Cartesian3.fromDegrees(
          params.lon, params.lat, params.h
        ),
        duration: params.duration || 0.0,
        orientation: {
          heading: params.heading || 0.0,
          pitch: params.pitch || 0.0,
          roll: params.roll || 0.0,
        }
      });
    }
  },
  /**
   * 绘制雷达罩
   * @param {string} lon 经度
   * @param {string} lat 纬度
   */
  bowensRadar(lon, lat, radii) {
    let a = viewer.entities.add({
      // id: "redBase2",
      position: Cesium.Cartesian3.fromDegrees(lon, lat),
      ellipsoid: {
        radii: new Cesium.Cartesian3(radii, radii, radii),
        maximumCone: Cesium.Math.toRadians(90),
        material: Cesium.Color.AQUAMARINE.withAlpha(0.1),
        outline: true,
        outlineColor: Cesium.Color.AQUAMARINE.withAlpha(0.1),
        outlineWidth: .5,
      },
    });
    return a
  },
  /**
   * 绘制 波形图
   * @param {string} lon 经度
   * @param {string} lat 纬度
   * @param {string} hei 高度
   * * @param {string} range 范围
   */
  drawRippleEnt(lon, lat, hei, range, color) {
    return viewer.entities.add({
      // id: 'wave' + id,
      position: Cesium.Cartesian3.fromDegrees(
        lon,
        lat,
      ),
      ellipse: this.drawRipple({
        type: "wave",
        semiMinorAxis: range,
        semiMajorAxis: range,
        height: hei,
        material: {
          color
        }
      }),
    });
  },
  // 绘制报警ellipse
  drawRipple(options) {
    let height = options.height || 0;
    let semiMinorAxis = options.semiMinorAxis || 10000;
    let semiMajorAxis = options.semiMajorAxis || 10000;
    if (!options.type) return;
    if (options.type === "wave") {
      //wave
      return {
        height,
        semiMinorAxis,
        semiMajorAxis,
        material: new CircleWaveMaterialPropertyP({
          duration: (options.material && options.material.duration) || 5000,
          color:
            (options.material && options.material.color) || Cesium.Color.GREEN,
          count: (options.material && options.material.count) || 1,
        }),
      };
    }
  },
  /**
   * 
   * @param {*} position 位置
   * @param {*} angle 角度
   * @param {*} length 扇形长度
   */
  //绘制多个扇形重叠
  land(position, angle, length, id) {
    let arrAll = []
      //计算扇形数组
    function computeCirclularFlight(lon, lat, radius) {
      let Ea = 6378137; //   赤道半径
      let Eb = 6356725; // 极半径 
      let positionArr = [];
      positionArr.push(lon);
      positionArr.push(lat);
      //需求正北是0° cesium正东是0°
      let jj = angle || 0;
      for (let i = jj; i <= jj + 70; i++) {
        let dx = radius * Math.sin(i * Math.PI / 180.0);
        let dy = radius * Math.cos(i * Math.PI / 180.0);

        let ec = Eb + (Ea - Eb) * (90.0 - lat) / 90.0;
        let ed = ec * Math.cos(lat * Math.PI / 180);

        let BJD = lon + (dx / ed) * 180.0 / (Math.PI);
        let BWD = lat + (dy / ec) * 180.0 / (Math.PI);

        positionArr.push(BJD);
        positionArr.push(BWD);
      }

      return positionArr;
    }
    let positionArr = computeCirclularFlight(position.lon, position.lat, length / 3);
    let positionArr2 = computeCirclularFlight(position.lon, position.lat, length / 1.5);
    let positionArr3 = computeCirclularFlight(position.lon, position.lat, length);

    let arr = [positionArr, positionArr2, positionArr3];
    let arrColor = [
        [246, 7, 7, 0.4],
        [246, 7, 7, 0.4],
        [246, 7, 7, 0.4]
      ]
      //绘制多个 组合扇形
    function draw(arr, colorA) {

      arr.forEach((val, i) => {
          let a = [];
          if (i == 0) {
            //数组第一个画完整的
            a = val;
          } else {
            //截取组合多边形
            let newArr = [];
            let frontArr = arr[i - 1].slice(2);
            for (let c = frontArr.length - 1; c >= 0; c -= 2) {
              newArr.push(frontArr[c - 1], frontArr[c])
            }
            a = newArr.concat(val.slice(2))
          }
          //发光线
          let lineArr = [],
            line = val.slice(2);
          for (let c = line.length - 1; c >= 0; c -= 2) {
            lineArr.push(Cesium.Cartesian3.fromDegrees(line[c - 1], line[c]))
          }
          let aa = viewer.entities.add({
            polyline: {
              positions: lineArr,
              width: 10.0,
              material: new Cesium.PolylineGlowMaterialProperty({
                color: new Cesium.Color(colorA[i][0] / 255, colorA[i][1] / 255, colorA[i][2] / 255, 1),
                glowPower: 0.25,
              }),
              clampToGround: true
            },
          });
          arrAll.push(aa)
            //面

          let bb = viewer.entities.add({
            polygon: {
              hierarchy: new Cesium.PolygonHierarchy(Cesium.Cartesian3.fromDegreesArray(
                a
              )),
              perPositionHeight: false,
              material: new Cesium.Color(colorA[i][0] / 255, colorA[i][1] / 255, colorA[i][2] / 255, colorA[i][3]),
              clampToGround: true
            }
          });
          arrAll.push(bb)
        })
        //画两边虚线
      let xian = arr[arr.length - 1].slice(2);
      let xuArr = [
        [xian[0], xian[1]],
        [xian[xian.length - 2], xian[xian.length - 1]]
      ];
      xuArr.forEach(val => {
        let posiArr = [];
        posiArr.push(Cesium.Cartesian3.fromDegrees(val[0], val[1]));
        posiArr.push(Cesium.Cartesian3.fromDegrees(position.lon, position.lat));
        let cc = viewer.entities.add({
          polyline: {
            positions: posiArr,
            width: 3,
            material: new Cesium.PolylineDashMaterialProperty({ //虚线
              color: Cesium.Color.CYAN
            }),
            clampToGround: true
          },

        });
        arrAll.push(cc)
      })
    }
    draw(arr, arrColor);
    return arrAll
  },
  //绘制流动波线
  addFlyLinesAndPoints(data) {
    let center = data.center; //起始点
    let cities = data.points; //可以为多组哦！
    if (data.flowing) {
      this.initPolylineTrailLinkMaterialProperty(data);
      data.options.polyline.material = new Cesium.PolylineTrailLinkMaterialProperty(data.options.polyline.material, data.options.polyline.interval);
    }
    for (let j = 0; j < cities.length; j++) {
      let points = this.parabolaEquation({ pt1: center, pt2: cities[j], height: data.height, num: 100 });
      let pointArr = [];
      for (let i = 0; i < points.length; i++) {
        pointArr.push(points[i][0], points[i][1], points[i][2]);
      }
      data.options.polyline.positions = Cesium.Cartesian3.fromDegreesArrayHeights(pointArr)
      viewer.entities.add(data.options);
    }
  },
  //抛物线方程
  parabolaEquation(options, resultOut) {
    // var positions1 = Cesium.Cartographic.fromDegrees(options.pt1.lon, options.pt1.lat);
    let height1 = options.minH
      // console.log(positions1)
      // var positions2 = Cesium.Cartographic.fromDegrees(options.pt1.lon, options.pt2.lat);
    let height2 = options.maxH
    let U = undefined;
    //方程 y=-(4h/L^2)*x^2+h h:顶点高度 L：横纵间距较大者
    const h = options.height || 500;
    const L = Math.abs(options.pt1.lon - options.pt2.lon) > Math.abs(options.pt1.lat - options.pt2.lat) ? Math.abs(options.pt1.lon - options.pt2.lon) : Math.abs(options.pt1.lat - options.pt2.lat);
    const num = options.num && options.num > 50 ? options.num : 50;
    const result = [];
    let dlt = L / num;
    if (Math.abs(options.pt1.lon - options.pt2.lon) > Math.abs(options.pt1.lat - options.pt2.lat)) { //以lon为基准
      const delLat = (options.pt2.lat - options.pt1.lat) / num;
      if (options.pt1.lon - options.pt2.lon > 0) {
        dlt = -dlt;
      }
      for (let i = 0; i < num; i++) {
        const tempH = h - Math.pow((-0.5 * L + Math.abs(dlt) * i), 2) * 4 * h / Math.pow(L, 2);
        const lon = options.pt1.lon + dlt * i;
        const lat = options.pt1.lat + delLat * i;
        tempH += height1
        if (i > 80) {
          if (tempH < height2) {
            tempH = height2
          }

        }
        result.push([lon, lat, tempH]);
      }
    } else { //以lat为基准
      let delLon = (options.pt2.lon - options.pt1.lon) / num;
      if (options.pt1.lat - options.pt2.lat > 0) {
        dlt = -dlt;
      }
      for (let i = 0; i < num; i++) {
        const tempH = h - Math.pow((-0.5 * L + Math.abs(dlt) * i), 2) * 4 * h / Math.pow(L, 2);
        const lon = options.pt1.lon + delLon * i;
        const lat = options.pt1.lat + dlt * i;
        tempH += height1
        if (i > 80) {
          if (tempH < height2) {
            tempH = height2
          }
        }
        result.push([lon, lat, tempH]);
      }
    }
    if (resultOut !== U) {
      resultOut = result;
    }
    // 落地
    // result.push([options.pt2.lon, options.pt2.lat, options.pt2.height || 0])
    return result;
  },
  //定义流动线
  initPolylineTrailLinkMaterialProperty(data) {
    let me = {};
    let U = undefined;

    function PolylineTrailLinkMaterialProperty(color, duration) {
      this._definitionChanged = new Cesium.Event();
      this._color = U;
      this._colorSubscription = U;
      this.color = color;
      this.duration = duration;
      this._time = (new Date()).getTime();
    }
    Object.defineProperties(PolylineTrailLinkMaterialProperty.prototype, {
      isConstant: {
        get: function() {
          return false;
        }
      },
      definitionChanged: {
        get: function() {
          return this._definitionChanged;
        }
      },
      color: Cesium.createPropertyDescriptor('color')
    });
    PolylineTrailLinkMaterialProperty.prototype.getType = function(time) {
      return 'PolylineTrailLink';
    }
    PolylineTrailLinkMaterialProperty.prototype.getValue = function(time, result) {
      if (!Cesium.defined(result)) {
        result = {};
      }
      result.color = Cesium.Property.getValueOrClonedDefault(this._color, time, Cesium.Color.WHITE, result.color);
      result.image = Cesium.Material.PolylineTrailLinkImage;
      result.time = (((new Date()).getTime() - this._time) % this.duration) / this.duration;
      return result;
    }
    PolylineTrailLinkMaterialProperty.prototype.equals = function(other) {
      return this === other || (other instanceof PolylineTrailLinkMaterialProperty && Property.equals(this._color, other._color))
    };
    Cesium.PolylineTrailLinkMaterialProperty = PolylineTrailLinkMaterialProperty;
    Cesium.Material.PolylineTrailLinkType = 'PolylineTrailLink';
    Cesium.Material.PolylineTrailLinkImage = data.flowImage; //图片
    Cesium.Material.PolylineTrailLinkSource = "czm_material czm_getMaterial(czm_materialInput materialInput)\n\
                                               {\n\
                                                    czm_material material = czm_getDefaultMaterial(materialInput);\n\
                                                    vec2 st = materialInput.st;\n\
                                                    vec4 colorImage = texture2D(image, vec2(fract(st.s - time), st.t));\n\
                                                    material.alpha = colorImage.a * color.a;\n\
                                                    material.diffuse = (colorImage.rgb+color.rgb)/2.0;\n\
                                                    return material;\n\
                                                }";
    // material.alpha:透明度;material.diffuse：颜色;
    Cesium.Material._materialCache.addMaterial(Cesium.Material.PolylineTrailLinkType, {
      fabric: {
        type: Cesium.Material.PolylineTrailLinkType,
        uniforms: {
          color: new Cesium.Color(1.0, 0.0, 0.0, 0.5),
          image: Cesium.Material.PolylineTrailLinkImage,
          time: 0
        },
        source: Cesium.Material.PolylineTrailLinkSource
      },
      translucent: function(material) {
        return true;
      }
    })
  }
}