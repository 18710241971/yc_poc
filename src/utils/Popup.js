export default class Popup {
  /**
   * 
   * @param { 
   *  content 元素内容
   *  class 样式类
   *  entity 实体
   *  close 关闭按钮
   * } parmas 
   */
  constructor(parmas) {
    this.id = parmas.id || parmas.entity.id;
    this.mountDom = parmas.content;
    this.top = parmas.top || 0;
    this.left = parmas.left || 0;
    //创建dom元素
    this.domEle = null;
    this.createEle(parmas)
      //挂载到entity 监听
    this.eventListener = this.render.bind(this, parmas.entity);
    viewer.clock.onTick.addEventListener(this.eventListener)
  }
  createEle(parmas) {
      let id = this.id;
      let dom = document.createElement("div");
      dom.style.position = 'absolute';
      dom.style.left = '0px';
      dom.style.top = '0px';
      dom.className = parmas.class;
      dom.innerHTML = `
         <div id=${'close' + id} class='popupClose' style='display:${parmas.close ? '' : 'none'};'>x</div>
         ${typeof this.mountDom == 'object' ? '' : this.mountDom}
        `;
      if (typeof this.mountDom == 'object') {
        this.mountDom.style.display = 'block';
        dom.appendChild(this.mountDom);
      }
      this.domEle = dom;
      viewer.container.append(this.domEle);
      //添加关闭事件
      if (parmas.close) {
        setTimeout(() => {
          let close = document.getElementById('close' + id);
          close.onclick = () => {
            this.close();
          }
        }, 200)
      }

    }
    //渲染位置
  render(entity) {
    this.domEle.style.display = 'block';
    if (!entity || entity.show == false) {
      this.domEle.style.display = 'none';
      return
    }
    let curtime = viewer.clock.currentTime;
    //笛卡尔坐标
    let positionEn = entity._position.getValue(curtime, null);
    let a = coordinate(positionEn);
    let c3 = getHeigthByLonLat(a.lng, a.lat)

    let position = Cesium.SceneTransforms.wgs84ToWindowCoordinates(viewer.scene, c3);
    if (position) {
      let left = position.x - (this.domEle.offsetWidth / 2 - this.left);
      let top = position.y - this.domEle.offsetHeight - this.top;
      let oleft = this.domEle.style.left.slice(0, -2);
      let otop = this.domEle.style.top.slice(0, -2);
      if (oleft - left > 1.2 || oleft - left < -1.2) {
        this.domEle.style.left = left + "px"
      }
      if (otop - top > 1.2 || otop - top < -1.2) {
        this.domEle.style.top = top + "px"
      }
    }
  }
  close() {
    if (typeof this.mountDom == 'object') {
      this.mountDom.style.display = 'none';
      viewer.container.append(this.mountDom);
    }
    this.domEle.remove();
    viewer.clock.onTick.removeEventListener(this.eventListener);
  }
}


function coordinate(item) {
  var cartesian33 = new Cesium.Cartesian3(item.x, item.y, item.z);
  var cartographic = Cesium.Cartographic.fromCartesian(cartesian33);
  var lat = Cesium.Math.toDegrees(cartographic.latitude);
  var lng = Cesium.Math.toDegrees(cartographic.longitude);
  return { lng: lng, lat: lat }
}

function getHeigthByLonLat(lon, lat) {
  var positions = Cesium.Cartographic.fromDegrees(lon, lat);
  // Cesium.sampleTerrain(viewer.terrainProvider, 11, [positions])
  //   .then((updatedPositions) => {
  //     updatedPositions.forEach((pos, i) => {
  //       h = pos.height
  //     });
  //   });
  let height = viewer.scene.globe.getHeight(positions);
  return Cesium.Cartesian3.fromDegrees(lon, lat, height);
}